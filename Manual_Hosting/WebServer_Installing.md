# WebServer Installing

После заказа хостинга для проектов NIT необходимо настроить веб-сервер. Подойдёт любой веб-сервер с поддержкой webdav. Однако компания рекомендует установить сервер на Линукс хостинг вместе с бесплатной панелью управления vesta, и вести настройку хостинга в ней.

После установки панели Vesta создайте в ней веб-сайт по-умолчанию, и добавьте IP адреса серверов в виде записей на DNS сервере. После этого в корне сервера создайте папки:

- WinUpdate — для пакета NIT-System-Update
- ExponentaForDebian — для пакета Exponenta в его Linux версии и зависимостей
- Exponenta — для пакета Exponenta for Windows и его зависимостей
- choco — для хостинга дополнительных пакетов Chocolatey
- Scripts — для скриптов автозагрузки пакетов с сайта

Внутри их создайте дерево каталогов, в соответствии с описанием кода соответствующих пакетов. Затем с помощью программ множественной замены текста по образцу внесите изменения в исходные файлы git репозитория, перекомпилируйте их для создания бинарных инсталляторов, и закачайте на сервер.

Для хостинга собственного репозитория Chocolatey необходимо установить сервер ProGet на Microsoft IIS (операционная система Windows). На эту же систему желательно установить и дедик NIT-Scheduler.

Веб-сервер IIS требует после установки некоторых глобальных настроек. По умолчанию в нём отсутствуют MIME типы для расширений .bat, .cmd, .ps1, .vb, .cs, .ini, .inf, .msi, .msu, .doc., .ion, .lnk, .reg, .sh, .md, .diff. Создайте MIME типы в соответствующей оснастки для этих расширений. ПРИМЕЧАНИЕ: MIME типы назначайте исходя из общих рекомендаций. В частности, наиболее общими типами являются "application/octet-stream" и "text/plain".

Также на веб-сервер IIS необходимо доустановить расширение "PHP Manager", а на компьютер установить одну из версий PHP. Это необходимо для работы некоторых скриптов.

Как Вы видите, для полноценной работы хосинга продуктов NIT требуется по меньшей мере два сервера, Windows и Linux. Оба из них можно установить на виртуальном хостинге.
