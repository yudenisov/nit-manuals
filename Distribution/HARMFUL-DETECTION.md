# Harmful Detection Обнаружение вредоносного содержимого

Представители компании New Internet Technologies Inc. уже устали повторять, что их программное обеспечение не содержит вирусов или потенциально опасного кода. Весь такой код убирается на поздних этапах тестирования продуктов. Наша миссия — написание грамотного кроссплатформенного кода, и Компания придерживается этой миссии лучше, чем многие свои транснациональные визави. Однако из-за многочисленных нападок на Компанию со стороны нечестных конкурентов, она решила выпустить следующий пресс-релиз.

## Юридические ограничения

Разработчики не имеют столь мощной юридической поддержки, как у многих современных IT компаний. Они больше нацелены на качество кода, чем на юридические возможности его использования. Именно поэтому конкуренты устраивают обструкцию компании, включая от попадания в чёрные списки Министерства торговли США до маски-шоу в офисе компании. Поэтому компания разработчик предупреждает, что не отвечает на клевету и злонамеренные выпады конкурентов и некоторых сотрудников правоохранительных/юридических органов. Компания не сотрудничает с национальными и международными киберпреступными группами, с террористическими организациями и со спецслужбами каких-либо государств, поскольку считает их абсолютным злом. Однако именно эти сообщества вставляют палки в колёса нашей Компании, объявляя производимое программное обеспечение нашей компании вредоносным, без уведомления и объяснения причин, но с немедленными юридическими санкциями. 

Вследствие этого Компании пришлось закрыть свой офис и перевести всех своих сотрудников на удалёнку. Более того, в начале октября 2021 года Компании пришлось приостановить тестирование своих продуктов и отозвать с рынка уже имеющееся программное обеспечение. Поэтому происки конкурентов разработчики оценивают очень серьёзно, но единственный способ противостоять этому давлению — это писать правильный код.

#### Ограничения вирусных сигнатур

В данной программе нет незаконного или вредоносного кода, хотя его потенциально возможно использовать для этих целей. Однако, с целью совместимости со стандартными технологиями Microsoft, код может содержать фрагменты, совпадающими с сигнатурами вирусов. Это не злонамеренные действия авторов кода, а недоработка программистов антивирусных компаний, а также происки конкурентов. В последних версиях антивирусов эти сигнатуры были исправлены, и теперь файлы библиотек могут детектироваться ими только как потенциально нежелательные программы, и не предлагается их удаление. Отнеситесь с пониманием к разработчикам!

Тем не менее некоторые файлы из этой библиотеки, прошедшие обфускацию, также были восприняты антивирусными системами как вредоносные программы. При этом в скриптах не было какого-то уникального кода, была только сделана попытка создать более короткую выжимку из кода библиотек. Единственно правдоподобная вероятная причина попадания кода в антивирусные базы данных — это глубокая его обфускация и сокрытие алгоритма работы. Но это было сделано с целью защиты от "выстрела в ногу", чтобы у не опытных пользователей не было соблазна редактировать файлы библиотеки без нужных навыков. Исходный же код библиотеки опубликован для исследователей. Сами понимаете, судиться с корпорацией Майкрософт и её приближёнными разработчики не могут, даже на 100% уверенные в своей правоте. Поэтому данный вопрос переводится из плоскости юридической в область техническую.

Основные же проблемы с установкой данных скриптов могут возникнуть при использовании на компьютерах средства защиты SmartScreen, который не разрешает скачивать и запускать сайты из неизвестных источников. Пользователям же можно посоветовать только игнорировать предупреждение SmartScreen стандартными способами.

#### Предложения по отключению антивирусной защиты

Исходя из всего вышесказанного, разработчики отчаялись найти справедливость и пошли по грубому и рекомендованному методу — рекомендовать пользователям и администраторам ограничивать антивирусную защиту для своих программ. Разработчики знают, какой сейчас поднимется шум и обвинения со стороны различных экспертов и конкурентов, но это мера вынужденная из-за действий недобросовестных конкурентов и является универсальным для такого рода проблем. Суть его — добавление исключений в антивирусные программы, без разницы каких. 

В настоящее время разработчиками проводятся исследования по рекомендованным исключениям для папок, процессов, типов файлов и т.п., с целью обеспечения бесперебойной работы как програмного обеспечения Компании, так и операционной системы Windows. Скорее всего, эти рекомендации никогда не попадут в открытый доступ, а будут распространяться только вместе с регистрацией продукта. Предполагается распространять продукт на условиях regesterware, чтобы сохранить контроль над распространением и модификацией продукта и защитить пользователей от обструкции.

## Исследователям антивирусного программного обеспечения

Данные скрипты обобщают все способы доставки файлов по технологии Download & Install. Данный код безопасен, но на его основе можно создавать вирусы, трооянцы и бэкдоры. Надеюсь, исследователи найдут способ обезвреживать такие программы, не занося базовый код библиотек в свои сигнатуры. В противном случае могут быть заблокированы многие инфраструктурные проекты и технологии, используемые при написании программ.

Хотя, как показывает практика 2018-2021 годов, конкуренты готовы пойти на любые преступления, чтобы получить прибыль, сохранить долю рынка и остаться у власти, и в этом им помогает государство. Их не останавливает ни отключение Интернета, ни ограничение конституционных свобод, ни даже ядерная война. Ведь продать верёвку бизнесмену сию секунду втридорога гораздо важнее, чем знать, что через минуту ты будешь на ней повешен!
